<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$template['active_template'] = 'default';


$template['default']['template'] = 'base';
$template['default']['regions'] = array( 
 'content',
 'extra_js' 
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE; 

//definimos una plantilla para el formulario de registro
$template['base']['template'] = 'base';
$template['base']['regions'] = array( 
 'content',
 'extra_js' 
);
$template['base']['parser'] = 'parser';
$template['base']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */