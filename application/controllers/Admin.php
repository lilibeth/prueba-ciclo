<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	 * 
	 */
	class Admin extends CI_Controller {
	 
	 public function __construct() {
		 parent::__construct();
		 $this->load->database('default');
		 $this->load->library(array('session'));
		 $this->load->library(array('form_validation'));
		 $this->load->helper(array('url','form'));
		 $this->load->model('visitas_model');
	 }
	 
	 public function index()
	 {
		 if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') != 'administrador')
		 {
		 	redirect(base_url().'login');
		 }
		 
		  // simple highcharts example
		 $graph_data = $this->_data();
			
		$this->load->library('highcharts');
	
		$this->highcharts->set_type('column'); // drauwing type
		$this->highcharts->set_title('Cantidad de visitas a clientes por ciudades', ''); // set chart title: title, subtitle(optional)
		$this->highcharts->set_axis_titles('Ciudades', 'Cantidad de visitas'); // axis titles: x axis,  y axis
		
		$this->highcharts->set_xAxis($graph_data['axis']); // pushing ciudades for x axis labels		
		$this->highcharts->set_serie($graph_data['popul']); // second serie
		
		
		
		$this->highcharts->render_to('my_div'); // choose a specific div to render to graph
		
		$data['charts'] = $this->highcharts->render();  
		 $this->template->set_template('default');
         $this->template->write_view('content', 'admin_view', $data, TRUE);
         $this->template->render();
	 }	 
	
	
	function _data()
	{		
		$visitas_por_ciudad = $this->visitas_model->get_visita_for_ciudad();
		$array_data = [];
		$array_nombres = [];
		foreach($visitas_por_ciudad as $row)
		{

		  $array_data[] = intval($row->cantidad);
		  $array_nombres[] = $row->nombre;
		};		
		$data['popul']['data'] = $array_data;
		$data['popul']['name'] = 'Cantidad de visitas';
		$data['axis']['categories'] = $array_nombres;
		
		return $data;
	}	

	public function report_visitas_for_cliente(){
		if($this->input->is_ajax_request())
            {
     
            	$this->form_validation->set_rules('nit', 'nit', 'trim|min_length[4]|required|max_length[255]');

            	$this->form_validation->set_message('required','El %s es obligatorio');
                $this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
                $this->form_validation->set_message('min_length', 'El %s no puede tener menos de %s carácteres');

                $this->form_validation->set_message('numeric', 'El %s tiene que ser numerico');
     
                if($this->form_validation->run() == FALSE)
                {
     
                	//de esta forma devolvemos los errores de formularios
                	//con ajax desde codeigniter, aunque con php es lo mismo
                	$errors = array(
                         'nit' => form_error('nit'),
                         'respuesta' => 'error'                         
                     );
                	//y lo devolvemos así para parsearlo con JSON.parse
                     echo json_encode($errors);
                     
                     return FALSE;
     
                }else{     
                	$nit = $this->input->post('nit');;
             	    $visita =  $this->visitas_model->get_visitas_for_cliente($nit);
                    
                 	$response = array(
                         'respuesta' => 'ok',
                         'visitas' => $visita,                         
                         'nombres' => $visita[0]->nombres,
                         'ciudad' => $visita[0]->nombre,
                         'saldo_cupo' => $visita[0]->saldo_cupo
                    );
                                	
                     echo json_encode($response);
     
                }
     
            }
         
	}
	
}