<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
Class Clientes extends CI_CONTROLLER
{
     public function __construct()
     {
         parent::__construct();
         
         //cargamos la base de datos por defecto
         $this->load->database('default');
         $this->load->library(array('session', 'form_validation', 'pagination'));
         //cargamos el helper url y el helper form
         $this->load->helper(array('url','form'));
         //cargamos la librería form_validation
         $this->load->library(array('form_validation'));
         //cargamos el modelo clientes_model
         $this->load->model('clientes_model');
         // cargamos el model paises_model
         $this->load->model('paises_model');
         // cargamos el model ciudades_model
         $this->load->model('ciudades_model');
         // cargamos el model departamentos_model
         $this->load->model('departamentos_model');
         
     
     }
     
     //cargamos la vista y pasamos el título y los clientes a
     //través del array data a la misma
     public function index($offset = 0)
     {
         if($this->session->userdata('perfil') == FALSE )
         {
            redirect(base_url().'login');
         }
         $limit = 2;
         $data = array(
            'titulo' => 'Lista clientes',           
            'clientes' => $this->clientes_model->paginacion($limit, $offset),
            'paises' => $this->paises_model->get_paises(),
            'ciudades' => $this->ciudades_model->get_ciudades(),
            'departamentos' => $this->departamentos_model->get_departamentos()
         );


          
          $total = $this->clientes_model->num_rows();          
          $config['base_url'] = base_url().'clientes/paginacion_ajax/';
          $config['total_rows'] = $total;
          $config['per_page'] = $limit;
          $config['uri_segment'] = '3';
          $this->pagination->initialize($config);
          $data['pag_links'] = $this->pagination->create_links();  
         
         $this->template->set_template('default');
         $this->template->write_view('content', 'clientes', $data, TRUE);
         $this->template->render();
         //$this->load->view('clientes',$data);
     }


     public function paginacion_ajax($offset = 0)
    {     

          $limit = 2; 

          $data = array(
            'titulo' => 'Lista clientes',           
            'clientes' => $this->clientes_model->paginacion($limit, $offset),
            'paises' => $this->paises_model->get_paises(),
            'ciudades' => $this->ciudades_model->get_ciudades(),
            'departamentos' => $this->departamentos_model->get_departamentos()
         );

          
          $total = $this->clientes_model->num_rows();          
          $config['base_url'] = base_url().'clientes/paginacion_ajax/';
          $config['total_rows'] = $total;
          $config['per_page'] = $limit;
          $config['uri_segment'] = '3';
          $this->pagination->initialize($config);
          $data['pag_links'] = $this->pagination->create_links();         
          $this->load->view('clientes', $data);

    }
     
     
     //función para eliminar clientes
     public function delete_clientes()
     {
            
            //comprobamos si es una petición ajax y existe la variable post id
            if($this->input->is_ajax_request() && $this->input->post('id'))
            {
     
            	$id = $this->input->post('id');
     
                $this->clientes_model->delete_clientes($id);
     
            }
     
     }
     
     //con esta función añadimos y editamos clientes dependiendo 
     //si llega la variable post id, en ese caso editamos
        public function multi_clientes()
        {
     
        	//comprobamos si es una petición ajax
        	if($this->input->is_ajax_request())
            {
     
            	$this->form_validation->set_rules('nit', 'nit', 'trim|min_length[4]|required|max_length[255]');

                $this->form_validation->set_rules('nombres', 'nombres', 'trim|min_length[4]|required|max_length[255]');

                $this->form_validation->set_rules('direccion', 'direccion', 'trim|min_length[4]|required|max_length[255]');

                $this->form_validation->set_rules('telefono', 'telefono', 'trim|min_length[4]|required|max_length[255]');

                $this->form_validation->set_rules('ciudad', 'ciudad', 'trim|min_length[1]|required|max_length[11]');

                $this->form_validation->set_rules('departamento', 'departamento', 'trim|min_length[1]|required|max_length[11]');

                $this->form_validation->set_rules('pais', 'pais', 'trim|min_length[1]|required|max_length[11]');

                $this->form_validation->set_rules('cupo', 'cupo', 'trim|min_length[4]|required|numeric');

                $this->form_validation->set_rules('saldo_cupo', 'saldo cupo', 'trim|min_length[4]|required|numeric');

                $this->form_validation->set_rules('porcentaje_visita', 'porcentaje visita', 'trim|min_length[1]|required|max_length[10]|decimal');
     
                $this->form_validation->set_message('required','El %s es obligatorio');
                $this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
                $this->form_validation->set_message('min_length', 'El %s no puede tener menos de %s carácteres');

                $this->form_validation->set_message('numeric', 'El %s tiene que ser numerico');
                $this->form_validation->set_message('numeric', 'El %s tiene que ser decimal');
     
                if($this->form_validation->run() == FALSE)
                {
     
                	//de esta forma devolvemos los errores de formularios
                	//con ajax desde codeigniter, aunque con php es lo mismo
                	$errors = array(
                         'nit' => form_error('nit'),
                         'nombres' => form_error('nombres'),
                         'direccion' => form_error('direccion'),
                         'telefono' => form_error('telefono'), 
                         'ciudad' => form_error('ciudad'),
                         'departamento' => form_error('departamento'), 
                         'pais' => form_error('pais'),
                         'cupo' => form_error('cupo'), 
                         'saldo_cupo' => form_error('saldo_cupo'),
                         'porcentaje_visita' => form_error('porcentaje_visita'),
                         'respuesta' => 'error'
                     );
                	//y lo devolvemos así para parsearlo con JSON.parse
                     echo json_encode($errors);
                     
                     return FALSE;
     
                }else{     
                	
             	    $data = array(             
                     'nit' => $this->input->post('nit'),
                     'nombres' => $this->input->post('nombres'),
                     'direccion' => $this->input->post('direccion'),
                     'telefono' => $this->input->post('telefono'), 
                     'ciudad' => $this->input->post('ciudad'),
                     'departamento' => $this->input->post('departamento'), 
                     'pais' => $this->input->post('pais'),
                     'cupo' => $this->input->post('cupo'), 
                     'saldo_cupo' => $this->input->post('saldo_cupo'),
                     'porcentaje_visita' => $this->input->post('porcentaje_visita')
                     );
     
             	    //si estamos editando
                	if($this->input->post('id'))
                	{
     
                         $id = $this->input->post('id');
                         $this->clientes_model->edit_clientes($id,$data);
             
                        	//si estamos agregando un cliente
                	}else{
     
                        $this->clientes_model->new_clientes($data);
     
                	}
             	
                 	//en cualquier caso damos ok porque todo ha salido bien
                 	//habría que hacer la comprobación de la respuesta del modelo
         
                 	$response = array(
                         'respuesta' => 'ok'
                    );
                                	
                     echo json_encode($response);
     
                }
     
            }
            
        }
 
}