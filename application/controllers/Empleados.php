<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
Class Empleados extends CI_CONTROLLER
{
     public function __construct()
     {
         parent::__construct();
         
         //cargamos la base de datos por defecto
         $this->load->database('default');
         $this->load->library(array('session'));
         //cargamos el helper url y el helper form
         $this->load->helper(array('url','form'));
         //cargamos la librería form_validation
         $this->load->library(array('form_validation', 'pagination'));
         //cargamos el modelo empleados_model
         $this->load->model('empleados_model');
     
     }
     
     //cargamos la vista y pasamos el título y los empleados a
     //través del array data a la misma
     public function index($offset = 0)
     {

         if($this->session->userdata('perfil') == FALSE )
         {
            redirect(base_url().'login');
         }
         $limit = 2; 
         $data = array(
            'empleados' => $this->empleados_model->paginacion($limit, $offset)
         );
          $total = $this->empleados_model->num_rows();          
          $config['base_url'] = base_url().'empleados/paginacion_ajax/';
          $config['total_rows'] = $total;
          $config['per_page'] = $limit;
          $config['uri_segment'] = '3';
          $this->pagination->initialize($config);
          $data['pag_links'] = $this->pagination->create_links(); 

         $this->template->set_template('default');
         $this->template->write_view('content', 'empleados', $data, TRUE);
         $this->template->render();
         //$this->load->view('empleados',$data);
     }


     public function paginacion_ajax($offset = 0)
    {     

          $limit = 2;          
         $data = array(
            'empleados' => $this->empleados_model->paginacion($limit, $offset)
         );
          
          $total = $this->empleados_model->num_rows();          
          $config['base_url'] = base_url().'empleados/paginacion_ajax/';
          $config['total_rows'] = $total;
          $config['per_page'] = $limit;
          $config['uri_segment'] = '3';
          $this->pagination->initialize($config);
          $data['pag_links'] = $this->pagination->create_links();         
          $this->load->view('empleados', $data);

    }
     
     
     //función para eliminar empleados
     public function delete_empleados()
     {
            
            //comprobamos si es una petición ajax y existe la variable post id
            if($this->input->is_ajax_request() && $this->input->post('id'))
            {
     
            	$id = $this->input->post('id');
     
                $this->empleados_model->delete_empleados($id);
     
            }
     
     }
     
     //con esta función añadimos y editamos empleados dependiendo 
     //si llega la variable post id, en ese caso editamos
        public function multi_empleados()
        {
     
        	//comprobamos si es una petición ajax
        	if($this->input->is_ajax_request())
            {
     
            	$this->form_validation->set_rules('nit', 'nit', 'trim|min_length[4]|required|max_length[20]');
                $this->form_validation->set_rules('nombres', 'nombres', 'trim|min_length[2]|required|max_length[255]');
     
                $this->form_validation->set_message('required','El %s es obligatorio');
                $this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
                $this->form_validation->set_message('min_length', 'El %s no puede tener menos de %s carácteres');
     
                if($this->form_validation->run() == FALSE)
                {
     
                	//de esta forma devolvemos los errores de formularios
                	//con ajax desde codeigniter, aunque con php es lo mismo
                	$errors = array(
                         'nit' => form_error('nombres'),
                         'nombres' => form_error('nit'),
                         'respuesta' => 'error'
                     );
                	//y lo devolvemos así para parsearlo con JSON.parse
                     echo json_encode($errors);
                     
                     return FALSE;
     
                }else{
     
                	$nit = $this->input->post('nit');
             	    $nombres = $this->input->post('nombres');
     
             	    //si estamos editando
                	if($this->input->post('id'))
                	{
     
                         $id = $this->input->post('id');
                         $this->empleados_model->edit_empleados($id,$nit,$nombres);
             
                        	//si estamos agregando un usuario
                	}else{
     
                        $this->empleados_model->new_empleados($nit,$nombres);
     
                	}
             	
                 	//en cualquier caso damos ok porque todo ha salido bien
                 	//habría que hacer la comprobación de la respuesta del modelo
         
                 	$response = array(
                         'respuesta' => 'ok'
                    );
                                	
                     echo json_encode($response);
     
                }
     
            }
            
        }
 
}