<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
Class Visitas extends CI_CONTROLLER
{
     public function __construct()
     {
         parent::__construct();
         
         //cargamos la base de datos por defecto
         $this->load->database('default');
         $this->load->library(array('session'));
         //cargamos el helper url y el helper form
         $this->load->helper(array('url','form'));
         //cargamos la librería form_validation
         $this->load->library(array('form_validation', 'pagination'));
         //cargamos el modelo clientes_model
         $this->load->model('clientes_model');
         // cargamos el model visitas_model
         $this->load->model('visitas_model');
         // cargamos el model empleados_model
         $this->load->model('empleados_model');
         
     
     }
     
     //cargamos la vista y pasamos el título y los clientes a
     //través del array data a la misma
     public function index($offset = 0)
     {

         if($this->session->userdata('perfil') == FALSE )
         {
            redirect(base_url().'login');
         }
         $limit = 2; 
         $data = array(                    
            'clientes' => $this->clientes_model->get_clientes(),
            'visitas' => $this->visitas_model->paginacion($limit, $offset),
            'vendedores' => $this->empleados_model->get_empleados()
         );
         $total = $this->visitas_model->num_rows();          
         $config['base_url'] = base_url().'visitas/paginacion_ajax/';
         $config['total_rows'] = $total;
         $config['per_page'] = $limit;
         $config['uri_segment'] = '3';
         $this->pagination->initialize($config);
         $data['pag_links'] = $this->pagination->create_links(); 
         $this->template->set_template('default');
         $this->template->write_view('content', 'visitas', $data, TRUE);
         $this->template->render();
         // $this->load->view('visitas',$data);
     }
     
     public function paginacion_ajax($offset = 0)
    {     

         $limit = 2;          
         $data = array(
            'clientes' => $this->clientes_model->get_clientes(),
            'visitas' => $this->visitas_model->paginacion($limit, $offset),
            'vendedores' => $this->empleados_model->get_empleados()
         );
          
         $total = $this->visitas_model->num_rows();          
         $config['base_url'] = base_url().'visitas/paginacion_ajax/';
         $config['total_rows'] = $total;
         $config['per_page'] = $limit;
         $config['uri_segment'] = '3';
         $this->pagination->initialize($config);
         $data['pag_links'] = $this->pagination->create_links();         
         $this->load->view('visitas', $data);

    }

     //función para eliminar visiatas
     public function delete_visitas()
     {
            
            //comprobamos si es una petición ajax y existe la variable post id
            if($this->input->is_ajax_request() && $this->input->post('id'))
            {
     
            	$id = $this->input->post('id');
     
                $this->visitas_model->delete_visitas($id);
     
            }
     
     }
     
     //con esta función añadimos y editamos visitas dependiendo 
     //si llega la variable post id, en ese caso editamos
        public function multi_visitas()
        {
     
        	//comprobamos si es una petición ajax
        	if($this->input->is_ajax_request())
            {
     
            	$this->form_validation->set_rules('fecha', 'fecha', 'trim|min_length[10]|required|max_length[10]|date');

                $this->form_validation->set_rules('vendedor', 'vendedor', 'trim|min_length[1]|required|max_length[255]');

                $this->form_validation->set_rules('cliente', 'cliente', 'trim|min_length[1]|required|max_length[255]');

                $this->form_validation->set_rules('observaciones', 'observaciones', 'trim|min_length[4]|required|max_length[255]');

                

                $this->form_validation->set_rules('valor_neto', 'valor neto', 'trim|min_length[2]|required|numeric');

                $this->form_validation->set_rules('valor_visita', 'valor visita', 'trim|min_length[2]|required|numeric');

                
     
                $this->form_validation->set_message('required','El %s es obligatorio');
                $this->form_validation->set_message('max_length', 'El %s no puede tener más de %s carácteres');
                $this->form_validation->set_message('min_length', 'El %s no puede tener menos de %s carácteres');
                $this->form_validation->set_message('numeric', 'El %s tiene que ser numerico');
     
                if($this->form_validation->run() == FALSE)
                {
     
                	//de esta forma devolvemos los errores de formularios
                	//con ajax desde codeigniter, aunque con php es lo mismo
                	$errors = array(
                         'fecha' => form_error('fecha'),
                         'vendedor' => form_error('vendedor'),
                         'cliente' => form_error('cliente'),
                         'observaciones' => form_error('observaciones'), 
                         'valor_neto' => form_error('valor_neto'),
                         'valor_visita' => form_error('valor_visita'),                          
                         'respuesta' => 'error'
                     );
                	//y lo devolvemos así para parsearlo con JSON.parse
                     echo json_encode($errors);
                     
                     return FALSE;
     
                }else{     
                	
             	    $data = array(             
                     'fecha' => $this->input->post('fecha'),
                     'vendedor' => $this->input->post('vendedor'),
                     'cliente' => $this->input->post('cliente'),                     
                     'valor_neto' => $this->input->post('valor_neto'),
                     'valor_visita' => $this->input->post('valor_visita'),
                     'observaciones' => $this->input->post('observaciones'),                       
                     );
     
             	    //si estamos editando
                	if($this->input->post('id'))
                	{
     
                         $id = $this->input->post('id');
                         $this->visitas_model->edit_visitas($id,$data);
             
                        	//si estamos agregando un visitas
                	}else{
     
                        $this->visitas_model->new_visitas($data);
     
                	}
             	
                 	//en cualquier caso damos ok porque todo ha salido bien
                 	//habría que hacer la comprobación de la respuesta del modelo
         
                 	$response = array(
                         'respuesta' => 'ok',
                         'data' => $data
                    );
                                	
                     echo json_encode($response);
     
                }
     
            }
            
        }
 
}