<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class Ciudades_model extends CI_Model {

		 public $nombre;
		 public $estado;
		 public $pais;
		 public $code;
	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos los paises
		 public function get_ciudades()
		 {
		 
		 	$query = $this->db->get('ciudades');
			if($query->num_rows() > 0)
			{
			 return $query->result();
			 
			}
		 
		 }
		 
		 	
}