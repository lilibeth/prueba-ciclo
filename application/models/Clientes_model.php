<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class Clientes_model extends CI_Model {


		  public $nit;
          public $nombres;
          public $direccion;
          public $telefono;
          public $ciudad;
          public $departamento;
          public $pais;
          public $cupo;
          public $saldo_cupo;
          public $porcentaje_visita;

	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos los clientes
		 public function get_clientes()
		 {
		 
		 	$query = $this->db->get('clientes');
			 if($query->num_rows() > 0)
			 {
			 
			 return $query->result();
			 
			 }
		 
		 }
		 
		 //eliminamos clientes
		 public function delete_clientes($id)
		 {
		 
			 $this->db->where('id',$id);
			 return $this->db->delete('clientes');
		 
		 }
		 
		 //editamos clientes
		 public function edit_clientes($id,$data)
		 {	 
			 $this->db->where('id',$id);
			 $this->db->update('clientes',$data);
		 
		 }
		 
		 //añadimos clientes
		 public function new_clientes($data)
		 {		 
			 $this->db->insert('clientes',$data);
		 
		 }


		 //obtenemos el total de filas para hacer la paginación
   		 public function num_rows() 
	    {
	    	
	        $consulta = $this->db->get('clientes');
	        return $consulta->num_rows();
			
	    }
	 
	    //obtenemos todos los posts a paginar con la función
	    //paginacion pasando la cantidad por página y el segmento
	    //como parámetros de la misma
	    public function paginacion($limit, $offset) 
	    {
	 
	        $consulta = $this->db->get('clientes', $limit, $offset);
	        if ($consulta->num_rows() > 0) 
	        {
	        	
	        	return $consulta->result();
			
			}
			
	    }		
}