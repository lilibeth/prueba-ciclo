<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class Departamentos_model extends CI_Model {

		 public $nombre;
	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos los paises
		 public function get_departamentos()
		 {
		 
		 	$query = $this->db->get('departamentos');
			if($query->num_rows() > 0)
			{
			 return $query->result();
			 
			}
		 
		 }
		 
		 	
}