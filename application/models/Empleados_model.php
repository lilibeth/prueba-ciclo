<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class Empleados_model extends CI_Model {

		 public $nit;
		 public $nombres_empleado;
	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos los empleados
		 public function get_empleados()
		 {
		 
		 	$query = $this->db->get('empleados');
			 if($query->num_rows() > 0)
			 {
			 
			 return $query->result();
			 
			 }
		 
		 }
		 
		 //eliminamos empleados
		 public function delete_empleados($id)
		 {
		 
			 $this->db->where('id',$id);
			 return $this->db->delete('empleados');
		 
		 }
		 
		 //editamos empleados
		 public function edit_empleados($id,$nit,$nombres)
		 {
		 
			 
			 $data = array(
			 
			 'nit' => $nit,
			 'nombres_empleado' => $nombres,
			 
			 );
			 
			 $this->db->where('id',$id);
			 $this->db->update('empleados',$data);
		 
		 }
		 
		 //añadimos empleados
		 public function new_empleados($nit,$nombres)
		 {
		 
			 
			 $data = array(
			 
			 'nit' => $nit,
			 'nombres_empleado' => $nombres,
			 
			 );
			 
			 $this->db->insert('empleados',$data);
		 
		 }	


		 //obtenemos el total de filas para hacer la paginación
   		 public function num_rows() 
	    {
	    	
	        $consulta = $this->db->get('empleados');
	        return $consulta->num_rows();
			
	    }
	 
	    //obtenemos todos los posts a paginar con la función
	    //paginacion pasando la cantidad por página y el segmento
	    //como parámetros de la misma
	    public function paginacion($limit, $offset) 
	    {
	 
	        $consulta = $this->db->get('empleados', $limit, $offset);
	        if ($consulta->num_rows() > 0) 
	        {
	        	
	        	return $consulta->result();
			
			}
			
	    }			
}