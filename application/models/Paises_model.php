<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class Paises_model extends CI_Model {

		 public $nombre;
		 public $iso_3166_1_a2;
		 public $iso_3166_1_a3;
		 public $iso_3166_1_numeric;
	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos los paises
		 public function get_paises()
		 {
		 
		 	$query = $this->db->get('paises');
			 if($query->num_rows() > 0)
			 {
			 
			 return $query->result();
			 
			 }
		 
		 }
		 
		 	
}