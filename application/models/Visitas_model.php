<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 * 
	 */
	class visitas_model extends CI_Model {


		  public $fecha;
          public $vendedor;
          public $cliente;
          public $valor_neto;
          public $valor_visita;
          public $observaciones;          

	 
		 public function __construct()
		 {
		 
		 	parent::__construct();
		 
		 }
		 
		 //obtenemos las visitas
		 public function get_visitas()
		 {
		    $this->db->select('*');
			$this->db->from('visitas');
			$this->db->join('clientes', 'visitas.cliente = clientes.id');
			$this->db->join('empleados', 'visitas.vendedor = empleados.id');
			$query = $this->db->get();
		 	// $query = $this->db->get('visitas');
			 if($query->num_rows() > 0)
			 {
			 
			 return $query->result();
			 
			 }
		 
		 }

		 public function get_visita_for_ciudad()
		 {
		    $this->db->select('COUNT(clientes.ciudad) AS cantidad, ciudades.nombre');
			$this->db->from('visitas');
			$this->db->join('clientes', 'visitas.cliente = clientes.id');
			$this->db->join('ciudades', 'clientes.ciudad = ciudades.id');
			$this->db->group_by('clientes.ciudad');			
			$query = $this->db->get();
			 if($query->num_rows() > 0)
			 {
			 
			 return $query->result();
			 
			 }

		 }

		 public function get_visitas_for_cliente($nit){
		 	$this->db->select('*');
			$this->db->from('visitas');
			$this->db->join('clientes', 'visitas.cliente = clientes.id');
			$this->db->join('empleados', 'visitas.vendedor = empleados.id');
			$this->db->join('ciudades', 'clientes.ciudad = ciudades.id');
		 	$this->db->where('clientes.nit',$nit);
		 	$query = $this->db->get();
			if($query->num_rows() > 0)
			 {
			 return $query->result();
			 }
		 }
		 
		 //eliminamos visitas
		 public function delete_visitas($id)
		 {
		 
			 $this->db->where('id_visita',$id);
			 return $this->db->delete('visitas');
		 
		 }
		 
		 //editamos visitas
		 public function edit_visitas($id,$data)
		 {	 

			 $this->db->where('id_visita',$id);
			 $this->db->update('visitas',$data);			 
			 $this->bajar_saldo_cupo($data["cliente"], $data['valor_visita']);
		 
		 }
		 
		 //añadimos visitas
		 public function new_visitas($data)
		 {		 
			 $this->db->insert('visitas',$data);
			 $this->bajar_saldo_cupo($data["cliente"], $data['valor_visita']);
		 
		 }	


		 public function bajar_saldo_cupo($id_cliente, $valor_visita)
		 {
             
             $this->db->where('id',$id_cliente);
             $query = $this->db->get('clientes');
             $cliente = $query->result();             
             $data = array(            
                     'saldo_cupo' => $cliente[0]->saldo_cupo - $valor_visita,
             ); 
             $this->db->where('id',$id_cliente);
			 $this->db->update('clientes',$data);
		 }	


		 //obtenemos el total de filas para hacer la paginación
   		 public function num_rows() 
	    {
	    	
	        $consulta = $this->db->get('visitas');
	        return $consulta->num_rows();
			
	    }
	 
	    //obtenemos todos los posts a paginar con la función
	    //paginacion pasando la cantidad por página y el segmento
	    //como parámetros de la misma
	    public function paginacion($limit, $offset) 
	    {
	        $this->db->select('*');
			$this->db->from('visitas');
			$this->db->join('clientes', 'visitas.cliente = clientes.id');
			$this->db->join('empleados', 'visitas.vendedor = empleados.id');
			$this->db->limit($limit, $offset);
	        $consulta = $this->db->get();
	        if ($consulta->num_rows() > 0) 
	        {
	        	
	        	return $consulta->result();
			
			}
			
	    }		
}