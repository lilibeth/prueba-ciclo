<div id="g_render"  class="left">
	<?php if (isset($charts)) echo $charts; ?>	
</div>
<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<br>
<div class="container_12">
    <h1>Reporte de visitas por cliente</h1>
    <div class="grid_12">
    	<div class="head-search">
			<form class="form" role="form" id='buscar' action="http://localhost/prueba-ciclo/admin/report_visitas_for_cliente" method="POST">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Nit cliente" name="nit">				  
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button" id='enviar'><i class="fa fa-search"></i></button>
				  </span>

				</div>
				<div class="respuesta respuesta_nit"></div>
			</form>
		</div>
    </div>
    <br>
       <h2 id='titulo_resultado'></h2>
    <br>
    <div class="grid_12">
        <div class="grid_12" id="head">
            <div class="grid_2" id="head_nombre">Fecha</div>
            <div class="grid_2" id="head_email">Vendedor</div>
            <div class="grid_2" id="head_email">saldo visita</div>                 
        </div>
        <div class="grid_12" id="body">
            <div class="grid_2" id="fecha">
            </div>
            <div class="grid_2" id="vendedor">                                       
            </div>
            <div class="grid_2" id="saldo_visita">
            </div>                           
        </div>
        <div class="grid_12" id="head">
            <div class="grid_2" id="head_nombre">----</div>
            <div class="grid_2" id="head_email">Total saldo visita</div>
            <div class="grid_2" id="head_total_saldo_visitas"></div>                 
        </div>
        <div class="grid_12" id="head">
            <div class="grid_2" id="head_nombre">----</div>
            <div class="grid_2" id="head_email">Total saldo cupo</div>
            <div class="grid_2" id="head_total_saldo_cupo"></div>                 
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>js/funciones_admin.js"></script>
