<!DOCTYPE html>
<html lang='es'>
    <head>
        <!-- Title here -->
        <title>ClicloMontaña S.</title>
        <meta charset="utf-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Styles -->
        <!-- Bootstrap CSS -->
        <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery gritter -->
        <link href="<?=base_url()?>css/jquery.gritter.css" rel="stylesheet">
        <!-- Font awesome CSS -->
        <link href="<?=base_url()?>css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom Color CSS -->
        <link href="<?=base_url()?>css/less-style.css" rel="stylesheet">    
        <!-- Custom CSS -->
        <link href="<?=base_url()?>css/style.css" rel="stylesheet">

         <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/960.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/text.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/reset.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/estilos.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.1/themes/ui-darkness/jquery-ui.css" /> 


        <!-- Javascript files -->
    <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap JS -->
        <script src="js/bootstrap.min.js"></script>
        <!-- jQuery UI -->
        <script src="js/jquery-ui.min.js"></script>
        <!-- Date Time Picker JS -->
        <script src="js/bootstrap-datetimepicker.min.js"></script>  
        <!-- Bootstrap wysihtml5 JS -->     
        <script src="js/wysihtml5-0.3.0.js"></script>
        <script src="js/prettify.js"></script>
        <script src="js/bootstrap-wysihtml5.min.js"></script>
    
        <!-- jQuery flot -->
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/jquery.flot.min.js"></script>     
        <script src="js/jquery.flot.stack.min.js"></script>
        <script src="js/jquery.flot.pie.min.js"></script>
        <script src="js/jquery.flot.resize.min.js"></script>
        
        <!-- Validate JS -->
        <script src="js/jquery.validate.js"></script>
        <!-- Form wizard steps  JS -->
        <script src="js/jquery.steps.min.js"></script>
        <!-- jQuery Knob -->
        <script src="js/jquery.knob.js"></script>
        <!-- jQuery Sparklines -->
        <script src="js/jquery.sparklines.js"></script>
        <!-- jQuery slim scroll -->
        <script src="js/jquery.slimscroll.min.js"></script>
        <!-- Data Tables JS -->
        <script src="js/jquery.dataTables.min.js"></script> 
        <!-- Pretty Photo JS -->
        <script src="js/jquery.prettyPhoto.js"></script>    
        <!-- Rate It JS -->
        <script src="js/jquery.rateit.min.js"></script> 
        <!-- Full calendar -->
        <script src="js/moment.min.js"></script>
        <script src="js/fullcalendar.min.js"></script>
                <!-- Respond JS for IE8 -->
        <script src="js/respond.min.js"></script>
        <!-- HTML5 Support for IE -->
        <script src="js/html5shiv.js"></script>
        
        
        <!-- Custom JS -->
        <script src="js/custom.js"></script>


       
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>      
       
    </head>
    
    <body>

        <div class="outer">
            
            <!-- Sidebar starts -->
            <div class="sidebar">
            
                <div class="sidey">
            
                    <!-- Logo -->
                    <div class="logo">
                        <h1><a href="#"></i> ClicloMontaña S.A.</a></h1>
                    </div>
                    
                    <!-- Sidebar navigation starts -->
                    
                    <!-- Responsive dropdown -->
                    <div class="sidebar-dropdown"><a href="#" class="br-red"><i class="fa fa-bars"></i></a></div>
                    
                    <div class="side-nav">
                    
                        <div class="side-nav-block">                            
                            <!-- Sidebar links -->
                            <ul class="list-unstyled">
                                <li>
                                    <a href="<?=base_url()?>admin" class="active"><i class="fa fa-signal"></i> Reportes</a>
                                </li>
                                <li>
                                    <a href="<?=base_url()?>empleados">
                                        <i class="fa fa-user"></i>Empleados</a>
                                    </li>
                                <li>
                                    <a href="<?=base_url()?>clientes">
                                        <i class="fa fa-user"></i> Clientes
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=base_url()?>visitas">
                                        <i class="fa fa-wrench"></i> Visitas
                                    </a>
                                </li>                              
                                
                            </ul>
                        </div>                  
                        
                    </div>
                    
                    <!-- Sidebar navigation ends -->
                    
                </div>
            </div>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">           
                <!-- Mainbar head starts -->
                <div class="main-head">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <!-- Page title -->
                                <h2><i class="fa fa-desktop lblue"></i> Reportes</h2>
                            </div>                          
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                
                            </div>                          
                            <div class="col-md-3 col-sm-4 hidden-xs">
                                
                            </div>                      
                            
                            <div class="col-md-3 hidden-sm hidden-xs">
                                <!-- Head user -->
                                <div class="head-user dropdown pull-right">
                                    <a href="#" data-toggle="dropdown" id="profile">                                                                      
                                        <!-- User name -->
                                        <?=$this->session->userdata('username')?><i class="fa fa-caret-down"></i> 
                                    </a>
                                    <!-- Dropdown -->
                                    <ul class="dropdown-menu" aria-labelledby="profile">
                                        <li><?=anchor(base_url().'login/logout', 'Cerrar sesión')?></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>  
                    </div>
                    
                </div>
                <!-- Mainbar head ends -->
                
                <div class="main-content">
                    <div class="container">
                                        
                        <div class="container_12">
                            <?= $content ?>
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
            
        </div>
        

        

        </body> 
</html>