<div id='content_clientes'>
        <div class="container_12" id='content_clientes'>
            <h1>Lista de emplados</h1>
            <div class="grid_12">
                <div class="grid_12" id="agregar"><input type="button" value="Añadir" id="" class="agregar"></div>
                <div class="grid_12" id="head">
                    <div class="grid_2" id="head_nombre">Nit</div>
                    <div class="grid_2" id="head_email">Nombres</div>
                    <div class="grid_2" id="head_email">Telefono</div>                 
                </div>
                <?php
                foreach($clientes as $fila):
                ?>
                    <div class="grid_12" id="body">
                        <div class="grid_2" id="nit<?=$fila->id?>">
                            <?=$fila->nit?>
                                
                        </div>
                        <div class="grid_2" id="nombres<?=$fila->id?>">
                            <?=$fila->nombres?>                        
                        </div>
                        <div class="grid_2" id="telefono<?=$fila->id?>">
                            <?=$fila->telefono?>
                                
                        </div>
                        <div class="grid_2" id="eliminar">
                            <input type="button" value="Eliminar" id="<?=$fila->id?>" class="eliminar">
                        </div>
                        <div class="grid_2" id="editar">
                            <input type="button" value="Editar" id="<?=$fila->id?>" class="editar">
                        </div> 
                        <input type="hidden" id="direccion<?=$fila->id?>" value="<?=$fila->direccion?>">
                        <input type="hidden" id="ciudad<?=$fila->id?>" value="<?=$fila->ciudad?>"> 
                        <input type="hidden" id="departamento<?=$fila->id?>" value="<?=$fila->departamento?>"> 
                        <input type="hidden" id="pais<?=$fila->id?>" value="<?=$fila->pais?>"> 
                        <input type="hidden" id="cupo<?=$fila->id?>" value="<?=$fila->cupo?>">
                        <input type="hidden" id="saldo_cupo<?=$fila->id?>" value="<?=$fila->saldo_cupo?>"> 
                        <input type="hidden" id="porcentaje_visita<?=$fila->id?>" value="<?=$fila->porcentaje_visita?>">                    
                    </div>
                <?php
                endforeach;
                ?>
                <div class="grid_12" id="head">
                    <ul id="pagination-digg">
                        <?=$pag_links;?>
                    </ul>
                </div>
                

            </div>
        </div>                 
                           
                

        <div class='insert_modal' id='dialog_create'>           
            <form name='insert' id='insert' method='post' action='http://localhost/prueba-ciclo/clientes/multi_clientes'>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Nit</label>
                        <input type='text' name='nit'  id='nit' /><br/>
                        <input type='hidden' name='id'  id='id' /><br/>
                        <div class='respuesta respuesta_nit'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Nombres</label>
                        <input type='text' name='nombres' id='nombres' /><br/>
                        <div class='respuesta respuesta_nombres'></div><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Dirreccion</label>
                        <input type='text' name='direccion' id='direccion' /><br/>
                        <div class='respuesta respuesta_direccion'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Telefono</label>
                        <input type='text' name='telefono'  id='telefono' /><br/>
                        <div class='respuesta respuesta_telefono'></div><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Ciudad</label>
                        <select name="ciudad" id="ciudad">
                            <?php
                              foreach($ciudades as $fila):
                            ?>
                              <option value="<?=$fila->id?>"><?=$fila->nombre?></option>
                            <?php
                              endforeach;
                            ?>
                        </select>
                        <div class='respuesta respuesta_ciudad'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Departamento</label>
                        <select name="departamento" id="departamento">
                            <?php
                              foreach($departamentos as $fila):
                            ?>
                              <option value="<?=$fila->id?>"><?=$fila->nombre?></option>
                            <?php
                              endforeach;
                            ?>
                        </select>
                        <div class='respuesta respuesta_departamento'></div><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Pais</label>                        
                        <select name="pais" id="pais">
                            <?php
                              foreach($paises as $fila):
                            ?>
                              <option value="<?=$fila->id?>"><?=$fila->nombre?></option>
                            <?php
                              endforeach;
                            ?>
                        </select>
                        <div class='respuesta respuesta_pais'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Cupo</label>
                        <input type='text' name='cupo'  id='cupo' /><br/>
                        <div class='respuesta respuesta_cupo'></div><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Saldo cupo</label>
                        <input type='text' name='saldo_cupo' id='saldo_cupo' /><br/>
                        <div class='respuesta respuesta_saldo_cupo'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Porcentaje de visita</label>
                        <input type='text' name='porcentaje_visita'  id='porcentaje_visita' /><br/>
                        <div class='respuesta respuesta_porcentaje_visita'></div><br/>
                    </div>
                </div>
            </form>     

        </div>
        <script type="text/javascript" src="<?php echo base_url()?>js/funciones_clientes.js"></script>
</div>
