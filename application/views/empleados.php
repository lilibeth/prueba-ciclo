<div id='content_clientes'>
    <div class="container_12">
    <h1>Lista de emplados</h1>
    <div class="grid_12">
        <div class="grid_12" id="agregar"><input type="button" value="Añadir" id="" class="agregar"></div>
        <div class="grid_12" id="head">
            <div class="grid_2" id="head_nombre">Nit</div>
            <div class="grid_3" id="head_email">Nombres</div>                
        </div>
        <?php
        foreach($empleados as $fila):
        ?>
            <div class="grid_12" id="body">
                <div class="grid_2" id="nit<?=$fila->id?>">
                    <?=$fila->nit?>
                        
                </div>
                <div class="grid_3" id="nombres<?=$fila->id?>">
                    <?=$fila->nombres_empleado?>                        
                </div>
                <div class="grid_2" id="eliminar">
                    <input type="button" value="Eliminar" id="<?=$fila->id?>" class="eliminar">
                </div>
                <div class="grid_2" id="editar">
                    <input type="button" value="Editar" id="<?=$fila->id?>" class="editar">
                </div>                   
            </div>
        <?php
        endforeach;
        ?>
        <div class="grid_12" id="head">
            <ul id="pagination-digg">
                <?=$pag_links;?>
            </ul>
        </div>
        

    </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url()?>js/funciones_empleados.js"></script>
</div>    

      