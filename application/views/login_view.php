<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>Login </title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="<?php echo base_url()?>css/font-awesome.min.css" rel="stylesheet">	
		<!-- Custom Color CSS -->
		<link href="<?php echo base_url()?>css/less-style.css" rel="stylesheet">	
		<!-- Custom CSS -->
		<link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/960.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/text.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/reset.css" media="screen" />
	</head>
	
	<body>

		<div class="outer-page">
			
			 <!-- Login page -->
			 <div class="login-page">

					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-justified">
					  <li class="active"><a href="#login" data-toggle="tab" class="br-lblue"><i class="fa fa-sign-in"></i> Iniciar sesion</a></li>					  
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane fade active in" id="login">
					  
						<!-- Login form -->
						
						
						<?php
						 $username = array('name' => 'username', 'placeholder' => 'nombre de usuario');
						 $password = array('name' => 'password', 'placeholder' => 'introduce tu password');
						 $submit = array('name' => 'submit', 'value' => 'Iniciar sesión', 'title' => 'Iniciar sesión');
						 ?>
						 
						 <?=form_open(base_url().'login/new_user')?>
						 <label for="username">Nombre de usuario:</label>
						 <?=form_input($username)?><p><?=form_error('username')?></p>
						 <label for="password">Introduce tu password:</label>
						 <?=form_password($password)?><p><?=form_error('password')?></p>
						 <?=form_hidden('token',$token)?>
						 <?=form_submit($submit)?>
						 <?=form_close()?>
						 <?php 
						 if($this->session->flashdata('usuario_incorrecto'))
						 {
						 ?>
						 <p><?=$this->session->flashdata('usuario_incorrecto')?></p>
						 <?php
						 }
						 ?>
									
						
					  </div>		  
					  			  
					
			 </div>	
			
		</div>		
		
	</body>	
</html>


