<div id='content_clientes'>
 <div class="container_12">
    <h1>Lista de visitas</h1>
    <div class="grid_12">
        <div class="grid_12" id="agregar"><input type="button" value="Añadir" id="" class="agregar"></div>
        <div class="grid_12" id="head">
            <div class="grid_2" id="head_nombre">Fecha</div>
            <div class="grid_2" id="head_email">Vendedor</div>
            <div class="grid_2" id="head_email">Cliente</div>                 
        </div>
        <?php
        foreach($visitas as $fila):
        ?>
            <div class="grid_12" id="body">
                <div class="grid_2" id="fecha<?=$fila->id_visita?>">
                    <?=$fila->fecha?>
                        
                </div>
                <div class="grid_2" id="vendedor_nombre<?=$fila->id_visita?>">
                    <?=$fila->nombres_empleado?>                        
                </div>
                <div class="grid_2" id="cliente<?=$fila->id_visita?>">
                    <?=$fila->nombres?>
                        
                </div>
                <div class="grid_2" id="eliminar">
                    <input type="button" value="Eliminar" id="<?=$fila->id_visita?>" class="eliminar">
                </div>
                <div class="grid_2" id="editar">
                    <input type="button" value="Editar" id="<?=$fila->id_visita?>" class="editar">
                </div> 
                <input type="hidden" id="valor_neto<?=$fila->id_visita?>" value="<?=$fila->valor_neto?>">
                <input type="hidden" id="valor_visita<?=$fila->id_visita?>" value="<?=$fila->valor_visita?>"> 
                <input type="hidden" id="observaciones<?=$fila->id_visita?>" value="<?=$fila->observaciones?>">
                <input type="hidden" id="vendedor<?=$fila->id_visita?>" value="<?=$fila->vendedor?>">
                <input type="hidden" id="clienteid<?=$fila->id_visita?>" value="<?=$fila->cliente?>">
                <input type="hidden" id="porcentaje_visita<?=$fila->id_visita?>" value="<?=$fila->porcentaje_visita?>">   
                                
            </div>
        <?php
        endforeach;
        ?>
        <div class="grid_12" id="head">
            <ul id="pagination-digg">
                <?=$pag_links;?>
            </ul>
        </div>
        

    </div>
</div>
<div class='insert_modal' id='dialog_create'>           
            <form name='insert' id='insert' method='post' action='http://localhost/prueba-ciclo/visitas/multi_visitas'>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Fecha</label>
                        <input type='date' name='fecha'  id='fecha' /><br/>
                        <input type='hidden' name='id'  id='id' /><br/>
                        <input type="hidden" id="porcentaje_visita" name='porcentaje_visita'> 
                        <div class='respuesta respuesta_fecha'></div><br/>
                    </div>
                    <div class="col-lg-6">                        
                        <label>Vendedor</label>
                        <select name="vendedor" id="vendedor">
                            <?php
                              foreach($vendedores as $fila):
                            ?>
                              <option value="<?=$fila->id?>"><?=$fila->nombres_empleado?></option>
                            <?php
                              endforeach;
                            ?>
                        </select>
                        <div class='respuesta respuesta_vendedor'></div><br/>
                    </div>
                </div>                
                <div class="row">                    
                    <div class="col-lg-6">
                        <label>Cliente</label>
                        <select name="cliente" id="cliente">
                            <?php
                              foreach($clientes as $fila):
                            ?>
                              <option value="<?=$fila->id?>" porcentaje_visita="<?=$fila->porcentaje_visita?>"><?=$fila->nombres?></option>
                            <?php
                              endforeach;
                            ?>
                        </select>
                        <div class='respuesta respuesta_cliente'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Valoir neto</label>
                        <input type='text' name='valor_neto' id='valor_neto' /><br/>
                        <div class='respuesta respuesta_valor_neto'></div><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">                                               
                        <label>Valoir visita</label>
                        <input type='text' name='valor_visita' id='valor_visita' /><br/>
                        <div class='respuesta respuesta_valor_visita'></div><br/>
                    </div>
                    <div class="col-lg-6">
                        <label>Observaciones</label>
                        <textarea name="observaciones" id="observaciones"></textarea>
                        <div class='respuesta respuesta_observaciones'></div><br/>
                    </div>
                </div>                
            </form>     
        
        </div>

<script type="text/javascript" src="<?php echo base_url()?>js/funciones_visitas.js"></script>
</div>