$(document).ready(function () {
    //editamos datos del clientes
    $("#enviar").on('click', function () {
        ajax_report();	      
    });

    $( "#buscar" ).submit(function( event ) {
	  ajax_report();
	  event.preventDefault();
	});
});

function ajax_report(){
	$.ajax({
	        url : $('#buscar').attr("action"),
	        type : $('#buscar').attr("method"),
	        data : $('#buscar').serialize(),
	        success:function (data) {

	            var obj = JSON.parse(data);

	            if(obj.respuesta == 'error')
	            {
	                
	                    $(".respuesta_nit").html(obj.nit);                    
	                    return false;

	            }else{

                    var array_total_visita = [];
	            	$.each(obj.visitas, function( index, value ) {
						$("#fecha").html(value.fecha);
		                $("#vendedor").html(value.vendedor);
		                $("#saldo_visita").html(value.valor_visita);
		                array_total_visita.push(value.valor_visita);
					});
					var sum = 0;
                    $.each(array_total_visita,function(){sum+=parseFloat(this) || 0;});
	                $(".respuesta_nit").html(' ');	                
	                var titulo = "Resultado de visitas del cliente " + obj.nombres +" recidente en la ciudad  " + obj.ciudad;
	                $("#titulo_resultado").html(titulo).attr('style', 'text-align:center;margin-top:30px');
	                $("#head_total_saldo_visitas").html(sum);
	                $("#head_total_saldo_cupo").html(obj.saldo_cupo);
	                return false;

	            }

	        }, error:function (error) {            
	            $("<div class='edit_modal'>Ha ocurrido un error!</div>").dialog({
	                resizable:false,
	                title:'Error editando!.',
	                height:200,
	                width:450,
	                modal:true
	            });
	        }

	    });
}