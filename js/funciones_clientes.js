$(document).ready(function () {
    //editamos datos del clientes
    $(".editar").on('click', function () {
 
        var id = $(this).attr('id');
        var nit = $.trim($("#nit" + id).html());
        var nombres = $.trim($("#nombres" + id).html());
        var direccion = $("#direccion" + id).val();
        var telefono = $.trim($("#telefono" + id).html());;
        var ciudad = $("#ciudad" + id).val();
        var departamento = $("#departamento" + id).val();
        var pais= $("#pais" + id).val();
        var cupo = $("#cupo" + id).val();
        var saldo_cupo = $("#saldo_cupo" + id).val();
        var porcentaje_visita = $("#porcentaje_visita" + id).val();

        $('#nit').val(nit);
        $('#nombres').val(nombres);
        $('#direccion').val(direccion);
        $('#telefono').val(telefono);        
        $('#ciudad > option[value="'+ciudad+'"]').attr('selected', 'selected');
        $('#departamento').val(departamento);
        $('#departamento > option[value="'+departamento+'"]').attr('selected', 'selected');
        $('#pais > option[value="'+pais+'"]').attr('selected', 'selected');
        $('#cupo').val(cupo);
        $('#saldo_cupo').val(saldo_cupo);
        $('#porcentaje_visita').val(porcentaje_visita);
        $('#id').val(id);
 
        $("#dialog_create").dialog({
 
                title:'Editar clientes.',
                
                buttons:{
                    
                    "Editar":function () {
                        $.ajax({
                            url : $('#insert').attr("action"),
                            type : $('#insert').attr("method"),
                            data : $('#insert').serialize(),
                            success:function (data) {
 
                                var obj = JSON.parse(data);
 
                                if(obj.respuesta == 'error')
                                {
                                    
                                        $(".respuesta_nit").html(obj.nit);
                                        $(".respuesta_nombres").html(obj.nombres);
                                        $(".respuesta_direccion").html(obj.direccion);
                                        $(".respuesta_telefono").html(obj.telefono);
                                        $(".respuesta_ciudad").html(obj.ciudad);
                                        $(".respuesta_departamento").html(obj.departamento);
                                        $(".respuesta_pais").html(obj.pais);
                                        $(".respuesta_cupo").html(obj.cupo);
                                        $(".respuesta_saldo_cupo").html(obj.saldo_cupo);
                                        $(".respuesta_porcentaje_visita").html(obj.porcentaje_visita);
                                        return false;
 
                                }else{
 
                                    $('#dialog_create').dialog("close");
 
                                    $("<div class='edit_modal'>El cliente fué editado correctamente</div>").dialog({
 
                                        resizable:false,
                                        title:'Cliente editado.',
                                        height:200,
                                        width:450,
                                        modal:true
 
                                    });
 
                                    setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/clientes";
                                    }, 2000);
 
                                }
 
                            }, error:function (error) {
                                $('#dialog_create').dialog("close");
                                $("<div class='edit_modal'>Ha ocurrido un error!</div>").dialog({
                                    resizable:false,
                                    title:'Error editando!.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                            }
 
                        });
                        return false;
                    },
                    Cancelar:function () {
                        $(this).dialog("close");
                        $("#insert")[0].reset();
                    }
                }
            });
        $("#dialog_create").dialog("open");
    });
 
    //eliminamos datos del clientes
    $(".eliminar").on('click', function () {
 
        var id = $(this).attr('id');
        var nombres = $("#nombres" + id).html();
 
        $("<div class='delete_modal'>¡Estás seguro que deseas eliminar al cliente " + nombres + "?</div>").dialog({
 
            resizable:false,
            title:'Eliminar al cliente ' + nombres + '.',
            height:200,
            width:450,
            modal:true,
            buttons:{
 
                "Eliminar":function () {
                    $.ajax({
                        type:'POST',
                        url:'http://localhost/prueba-ciclo/clientes/delete_clientes',
                        async: true,
                        data: { id : id },
                        complete:function () {
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>El cliente " + nombres + " fué eliminado correctamente</div>").dialog({
                                resizable:false,
                                title:'Cliente eliminado.',
                                height:200,
                                width:450,
                                modal:true
                            });
 
                            setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/clientes";
                                    }, 2000);
 
                        }, error:function (error) {
 
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error eliminando al cliente!.',
                                height:200,
                                width:550,
                                modal:true
 
                            });
 
                        }
 
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                }
            }
        });
    });
 
    //añadimos clientes nuevos
    $(".agregar").on('click', function () {
 
        var id = $(this).attr('id');        
 
        $("#dialog_create").dialog({ 
            
            buttons:{
 
                "Insertar":function () {
                    $.ajax({
                        url : $('#insert').attr("action"),
                        type : $('#insert').attr("method"),
                        data : $('#insert').serialize(),
 
                        success:function (data) {
 
                            var obj = JSON.parse(data);
 
                            if(obj.respuesta == 'error')
                            {
                                    
                                    $(".respuesta_nit").html(obj.nit);
                                    $(".respuesta_nombres").html(obj.nombres);
                                    $(".respuesta_direccion").html(obj.direccion);
                                    $(".respuesta_telefono").html(obj.telefono);
                                    $(".respuesta_ciudad").html(obj.ciudad);
                                    $(".respuesta_departamento").html(obj.departamento);
                                    $(".respuesta_pais").html(obj.pais);
                                    $(".respuesta_cupo").html(obj.cupo);
                                    $(".respuesta_saldo_cupo").html(obj.saldo_cupo);
                                    $(".respuesta_porcentaje_visita").html(obj.porcentaje_visita);
                                    return false;
 
                             }else{
 
                                $('#dialog_create').dialog("close");
                                $("<div class='insert_modal'>El cliente fué añadido correctamente</div>").dialog({
                                    resizable:false,
                                    title:'Cliente añadido.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                                setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/clientes";
                                    }, 2000);
                            }
 
                        }, error:function (error) {
                            $('#dialog_create').dialog("close");
                            $("<div class='insert_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error añadiendo!.',
                                height:200,
                                width:450,
                                modal:true
                            });
                        }
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                    $("#insert")[0].reset();
                }
            }
        });
        $("#dialog_create").dialog("open");
    });


    $("#dialog_create").dialog({
 
            resizable:false,
            title:'Añadir nuevo cliente.',
            height:600,
            width:600,
            modal:true,
            autoOpen: false,
            
    });

    $(document).on("click", "#pagination-digg a", function(event){
         event.preventDefault();
         var href = $(this).attr("href");
         $("#content_clientes").load(href);         
    }); 
});