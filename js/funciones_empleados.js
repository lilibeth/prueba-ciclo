$(document).ready(function () {
    //editamos datos del empleado
    $(".editar").on('click', function () {
 
        var id = $(this).attr('id');
        var nit = $.trim($("#nit" + id).html());
        var nombres = $.trim($("#nombres" + id).html());
 
        $("<div class='edit_modal'><form name='edit' id='edit' method='post' action='http://localhost/prueba-ciclo/empleados/multi_empleados'>"+
            "<label>Nit</label><input type='text' name='nit' class='nombre' value='"+nit+"' id='nit' /><br/>"+
            "<input type='hidden' name='id' class='id' id='id' value="+id+">"+
            "<label>Nombres</label><input type='text' name='nombres' class='email' value='"+nombres+"' id='nombres' /><br/>"+
            "</form><div class='respuesta'></div></div>").dialog({
 
                resizable:false,
                title:'Editar empleados.',
                height:300,
                width:450,
                modal:true,
                buttons:{
                    
                    "Editar":function () {
                        $.ajax({
                            url : $('#edit').attr("action"),
                            type : $('#edit').attr("method"),
                            data : $('#edit').serialize(),
 
                            success:function (data) {
 
                                var obj = JSON.parse(data);
 
                                if(obj.respuesta == 'error')
                                {
                                    
                                        $(".respuesta").html(obj.nit + '<br />' + obj.nombres);
                                        return false;
 
                                }else{
 
                                    $('.edit_modal').dialog("close");
 
                                    $("<div class='edit_modal'>El usuario fué editado correctamente</div>").dialog({
 
                                        resizable:false,
                                        title:'Usuario editado.',
                                        height:200,
                                        width:450,
                                        modal:true
 
                                    });
 
                                    setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/empleados";
                                    }, 2000);
 
                                }
 
                            }, error:function (error) {
                                $('.edit_modal').dialog("close");
                                $("<div class='edit_modal'>Ha ocurrido un error!</div>").dialog({
                                    resizable:false,
                                    title:'Error editando!.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                            }
 
                        });
                        return false;
                    },
                    Cancelar:function () {
                        $(this).dialog("close");
                    }
                }
            });
    });
 
    //eliminamos datos del empleado
    $(".eliminar").on('click', function () {
 
        var id = $(this).attr('id');
        var nombres = $("#nombres" + id).html();
 
        $("<div class='delete_modal'>¡Estás seguro que deseas eliminar al usuario " + nombres + "?</div>").dialog({
 
            resizable:false,
            title:'Eliminar al usuario ' + nombres + '.',
            height:200,
            width:450,
            modal:true,
            buttons:{
 
                "Eliminar":function () {
                    $.ajax({
                        type:'POST',
                        url:'http://localhost/prueba-ciclo/empleados/delete_empleados',
                        async: true,
                        data: { id : id },
                        complete:function () {
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>El usuario " + nombres + " fué eliminado correctamente</div>").dialog({
                                resizable:false,
                                title:'Usuario eliminado.',
                                height:200,
                                width:450,
                                modal:true
                            });
 
                            setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/empleados";
                                    }, 2000);
 
                        }, error:function (error) {
 
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error eliminando al usuario!.',
                                height:200,
                                width:550,
                                modal:true
 
                            });
 
                        }
 
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                }
            }
        });
    });
 
    //añadimos empleados nuevos
    $(".agregar").on('click', function () {
 
        var id = $(this).attr('id');
 
        var nombre = $("#nombre" + id).html();
 
        $("<div class='insert_modal'><form name='insert' id='insert' method='post' action='http://localhost/prueba-ciclo/empleados/multi_empleados'>"+
            "<label>Nit</label><input type='text' name='nit' class='nombre' id='nit' /><br/>"+
            "<label>Nombres</label><input type='text' name='nombres' class='email' id='nombres' /><br/>"+
            "</form><div class='respuesta'></div></div>").dialog({
 
            resizable:false,
            title:'Añadir nuevo empleado.',
            height:300,
            width:450,
            modal:true,
            buttons:{
 
                "Insertar":function () {
                    $.ajax({
                        url : $('#insert').attr("action"),
                        type : $('#insert').attr("method"),
                        data : $('#insert').serialize(),
 
                        success:function (data) {
 
                            var obj = JSON.parse(data);
 
                            if(obj.respuesta == 'error')
                            {
                                    
                                    $(".respuesta").html(obj.nit + '<br />' + obj.nombres);
                                    return false;
 
                             }else{
 
                                $('.insert_modal').dialog("close");
                                $("<div class='insert_modal'>El usuario fué añadido correctamente</div>").dialog({
                                    resizable:false,
                                    title:'Usuario añadido.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                                setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/empleados";
                                    }, 2000);
                            }
 
                        }, error:function (error) {
                            $('.insert_modal').dialog("close");
                            $("<div class='insert_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error añadiendo!.',
                                height:200,
                                width:450,
                                modal:true
                            });
                        }
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                }
            }
        });
    });

    $(document).on("click", "#pagination-digg a", function(event){
         event.preventDefault();
         var href = $(this).attr("href");
         $("#content_clientes").load(href);         
    }); 
});