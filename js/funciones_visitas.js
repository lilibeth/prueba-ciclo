$(document).ready(function () {
    //editamos datos del visitas
    $(".editar").on('click', function () {
 
        var id = $(this).attr('id');
        var fecha = $.trim($("#fecha" + id).html());
        var vendedor = $("#vendedor" + id).val();
        var cliente = $.trim($("#clienteid" + id).val());
        var valor_neto = $("#valor_neto" + id).val();;
        var valor_visita = $("#valor_visita" + id).val();
        var observaciones = $("#observaciones" + id).val();
        var porcentaje_visita =  $("#porcentaje_visita" + id).val();       

        $('#fecha').val(fecha);
        $('#vendedor').val(vendedor);
        $('#vendedor > option[value="'+vendedor+'"]').attr('selected', 'selected');
        $('#cliente > option[value="'+cliente+'"]').attr('selected', 'selected');
        $('#valor_neto').val(valor_neto);
        $('#valor_visita').val(valor_visita);
        $('#observaciones').val(observaciones);        
        $('#id').val(id);
        $('#porcentaje_visita').val(porcentaje_visita);
        
 
        $("#dialog_create").dialog({
 
                title:'Editar visitas.',
                
                buttons:{
                    
                    "Editar":function () {
                        $.ajax({
                            url : $('#insert').attr("action"),
                            type : $('#insert').attr("method"),
                            data : $('#insert').serialize(),
                            success:function (data) {
 
                                var obj = JSON.parse(data);
 
                                if(obj.respuesta == 'error')
                                {
                                    
                                        $(".respuesta_fecha").html(obj.fecha);
                                        $(".respuesta_vendedor").html(obj.vendedor);
                                        $(".respuesta_cliente").html(obj.cliente);
                                        $(".respuesta_valor_neto").html(obj.valor_neto);
                                        $(".respuesta_valor_visita").html(obj.valor_visita);
                                        $(".respuesta_observaciones").html(obj.observaciones); 
                                        return false;
 
                                }else{
 
                                    $('#dialog_create').dialog("close");
 
                                    $("<div class='edit_modal'>La visita fué editado correctamente</div>").dialog({
 
                                        resizable:false,
                                        title:'Visita editado.',
                                        height:200,
                                        width:450,
                                        modal:true
 
                                    });
 
                                    setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/visitas";
                                    }, 2000);
 
                                }
 
                            }, error:function (error) {
                                $('#dialog_create').dialog("close");
                                $("<div class='edit_modal'>Ha ocurrido un error!</div>").dialog({
                                    resizable:false,
                                    title:'Error editando!.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                            }
 
                        });
                        return false;
                    },
                    Cancelar:function () {
                        $(this).dialog("close");
                        $("#insert")[0].reset();
                    }
                }
            });
        $("#dialog_create").dialog("open");
    });
 
    //eliminamos datos del visitas
    $(".eliminar").on('click', function () {
 
        var id = $(this).attr('id');
        var nombres = $("#fecha" + id).html() + ' ' + $("#vendedor" + id).html() + ' ' + $("#cliente" + id).html();
 
        $("<div class='delete_modal'>¡Estás seguro que deseas eliminar la visita " + nombres + "?</div>").dialog({
 
            resizable:false,
            title:'Eliminar la visita ' + nombres + '.',
            height:200,
            width:450,
            modal:true,
            buttons:{
 
                "Eliminar":function () {
                    $.ajax({
                        type:'POST',
                        url:'http://localhost/prueba-ciclo/visitas/delete_visitas',
                        async: true,
                        data: { id : id },
                        complete:function () {
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>La visita " + nombres + " fué eliminado correctamente</div>").dialog({
                                resizable:false,
                                title:'Visita eliminado.',
                                height:200,
                                width:450,
                                modal:true
                            });
 
                            setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/visitas";
                                    }, 2000);
 
                        }, error:function (error) {
 
                            $('.delete_modal').dialog("close");
                            $("<div class='delete_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error eliminando al visita!.',
                                height:200,
                                width:550,
                                modal:true
 
                            });
 
                        }
 
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                }
            }
        });
    });
 
    //añadimos visitas nuevos
    $(".agregar").on('click', function () {
 
        var id = $(this).attr('id');        
 
        $("#dialog_create").dialog({ 
            
            buttons:{
 
                "Insertar":function () {
                    $.ajax({
                        url : $('#insert').attr("action"),
                        type : $('#insert').attr("method"),
                        data : $('#insert').serialize(),
 
                        success:function (data) {
 
                            var obj = JSON.parse(data);
 
                            if(obj.respuesta == 'error')
                            {
                                    
                                    $(".respuesta_fecha").html(obj.fecha);
                                    $(".respuesta_vendedor").html(obj.vendedor);
                                    $(".respuesta_cliente").html(obj.cliente);
                                    $(".respuesta_valor_neto").html(obj.valor_neto);
                                    $(".respuesta_valor_visita").html(obj.valor_visita);
                                    $(".respuesta_observaciones").html(obj.observaciones);                                    
                                    return false;
 
                             }else{
 
                                $('#dialog_create').dialog("close");
                                $("<div class='insert_modal'>La visita fué añadido correctamente</div>").dialog({
                                    resizable:false,
                                    title:'Visita añadida.',
                                    height:200,
                                    width:450,
                                    modal:true
                                });
                                setTimeout(function() {
                                        window.location.href = "http://localhost/prueba-ciclo/visitas";
                                    }, 2000);
                            }
 
                        }, error:function (error) {
                            $('#dialog_create').dialog("close");
                            $("<div class='insert_modal'>Ha ocurrido un error!</div>").dialog({
                                resizable:false,
                                title:'Error añadiendo!.',
                                height:200,
                                width:450,
                                modal:true
                            });
                        }
                    });
                    return false;
                },
                Cancelar:function () {
                    $(this).dialog("close");
                    $("#insert")[0].reset();
                }
            }
        });
        $("#dialog_create").dialog("open");
    });


    $("#dialog_create").dialog({
 
            resizable:false,
            title:'Añadir nueva visits.',
            height:600,
            width:600,
            modal:true,
            autoOpen: false,
            
        });

    $(document).on('change', '#valor_neto', function(){
        var valor_visita = $('#valor_neto').val() * $('#porcentaje_visita').val();
        $("#valor_visita").val(valor_visita);
    });


    $(document).on('change', '#cliente', function(){
        var porcentaje_visita = $("#cliente option:selected").attr('porcentaje_visita');
        $("#porcentaje_visita").val(porcentaje_visita);
    });


    $(document).on("click", "#pagination-digg a", function(event){
         event.preventDefault();
         var href = $(this).attr("href");
         $("#content_clientes").load(href);         
    }); 
});